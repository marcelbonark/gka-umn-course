﻿using UnityEngine;

public class MeshGenerator : MonoBehaviour
{
    //Declaring and initializing values for our mesh
    float width = 1.0f;
    float height = 1.0f;
    float thick = 1.0f;

    // Start is called before the first frame update
    void Start()
    {
        Mesh mesh = new Mesh();
        var vertices = new Vector3[8];

        //First Layer of 4-Vertices Quad
        vertices[0] = new Vector3(-width, -height, thick);
        vertices[1] = new Vector3(-width, height, thick);
        vertices[2] = new Vector3(width, height, thick);
        vertices[3] = new Vector3(width, -height, thick);

        //Second Layer of 4-Vertices Quad
        vertices[4] = new Vector3(-width, -height, -thick);
        vertices[5] = new Vector3(-width, height, -thick);
        vertices[6] = new Vector3(width, height, -thick);
        vertices[7] = new Vector3(width, -height, -thick);

        mesh.vertices = vertices;

        // Create new colors array where the colors will be created.
        Color32[] colors = new Color32[vertices.Length];

        for (int i = 0; i < vertices.Length - 2; i += 3)
        {
            colors[i] = new Color32(255, 0, 0, 255);
            colors[i + 1] = new Color32(0, 255, 0, 255);
            colors[i + 2] = new Color32(0, 0, 255, 255);
        }
        mesh.colors32 = colors;

        mesh.triangles = new int[] {
            2, 1, 0,
            3, 2, 0, //First Face

            3, 0, 4,
            4, 7, 3, //Second Face

            3, 6, 2,
            3, 7, 6,  //Third Face

            6,5,2,
            5,1,2, //Top Face

            5,6,4,
            6,7,4, //Back Face
            
            1,5,4,
            4,0,1
        };
        GetComponent<MeshFilter>().mesh = mesh;

    }
}
